require 'mysql2'

class MysqlBackend
  def initialize
    @client = Mysql2::Client.new(host: 'localhost', username: '-', password: '-', database: 'inc_test')
  end

  def get_row(key)
    query = "SELECT * FROM impressions WHERE portal='#{key[0]}' AND item_id=#{key[1]}"
    @client.query(query)
  end

  def exec_batch(batch)
    return [] if batch.empty?

    compacted = batch.reduce({}) do |acc, item|
      acc[item] ||= 0
      acc[item] += 1
      acc
    end

    values = compacted.map do |key, impressions|
      portal, item_id, user_id, date = key
      "('#{portal}', #{item_id}, #{user_id}, '#{date}', #{impressions})"
    end.join(',')

    query = "INSERT INTO impressions(portal, item_id, user_id, date, impressions) VALUES #{values} ON DUPLICATE KEY UPDATE "\
      "impressions = impressions + VALUES(impressions)"

    begin
      @client.query(query)
    rescue Mysql2::Error => e
      puts e.message
      retry if e.message =~ "Deadlock"
    end
    []
  end

  def close
    @client.close
  end
end
