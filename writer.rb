require './stats'
require './key'
require './mysql_backend'
require './redis_backend'
require './memory_backend'

THREADS = 20
BATCH = 100

BACKEND_CLASS = MysqlBackend

stats = Stats.new(name: "#{BACKEND_CLASS} writer", span: 10)

threads = []
  THREADS.times do
    threads << Thread.new do
      backend = BACKEND_CLASS.new

      batch = []
      loop do
        batch << random_key
        stats.stat

        next if batch.size < BATCH
        batch = backend.exec_batch(batch)
      end
      exec_batch(client, batch)
      client.close
    end
end

threads.each { |t| t.join }
