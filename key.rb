ITEM_IDS = {
  'de' => 30_000_000,
  'fr' => 10_000_000,
  'us' => 10_000_000,
  'uk' => 1_000_000,
}

def random_key
  portal = ITEM_IDS.keys.sample
  id = rand(ITEM_IDS[portal]) + 1
  user_id = (id + 4234324) % 10_000
  date = Time.now - rand(604800)
  [portal, id, user_id, date.strftime('%Y-%m-%d')]
end
