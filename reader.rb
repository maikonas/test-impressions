require './stats'
require './key'
require './mysql_backend'
require './redis_backend'
require './memory_backend'

THREADS = 20
BACKEND_CLASS = MysqlBackend

stats = Stats.new(name: "#{BACKEND_CLASS} reader", span: 10)

threads = []
  THREADS.times do
    threads << Thread.new do
      backend = BACKEND_CLASS.new

      loop do
        backend.get_row(random_key)
        stats.stat
      end

      client.close
    end
end

threads.each { |t| t.join }
