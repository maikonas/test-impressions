
class Stats
  def initialize(span:, name:)
    @mutex = Mutex.new
    @span = span
    @start = Time.now
    @total = 0
    @now = Time.now
    @name = name
  end

  def stat
    @mutex.synchronize do
      @total += 1

      current = Time.now
      if current - @now > @span
        puts "#{@name} throughput #{@total / (current - @start)} per sec."
        @now = current
      end
    end
  end
end
