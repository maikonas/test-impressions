require 'mysql2'

SHARED_MEM = {}

class MemoryBackend

  def get_row(key)
    raise "cannot get row for memory backend"
  end

  def exec_batch(batch)
    return [] if batch.empty?

    batch.each do |key|
      full_key = [key[0], key[2], key[1]]
      SHARED_MEM[full_key] ||= {}
      SHARED_MEM[full_key][key[3]] ||= 0
      SHARED_MEM[full_key][key[3]] += 1
    end
    []
  end

  def close
  end
end
