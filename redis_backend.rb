require 'redis'

class RedisBackend
  def initialize
    @client = Redis.new(:url => "redis://localhost/13")
  end

  def get_row(key)
    @client.hgetall("#{key[0]}-#{key[2]}-#{key[1]}")
  end

  def exec_batch(batch)
    return [] if batch.empty?

    @client.pipelined do
      batch.each do |key|
        @client.hincrby("#{key[0]}-#{key[2]}-#{key[1]}", key[3], 1)
      end
    end
    []
  end

  def close
    @client.close
  end
end
